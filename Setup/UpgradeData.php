<?php


namespace PushON\SubcategoryWidget\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;

class UpgradeData implements UpgradeDataInterface
{

    private $eavSetupFactory;

    /**
     * Constructor
     *
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        if (version_compare($context->getVersion(), "1.0.1", "<")) {

            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Category::ENTITY,
                'root_thumbnail',
                [
                    'type' => 'varchar',
                    'label' => 'root_thumbnail',
                    'input' => 'image',
                    'sort_order' => 333,
                    'source' => '',
                    'global' => 0,
                    'visible' => true,
                    'required' => true,
                    'user_defined' => false,
                    'default' => null,
                    'group' => 'General Information',
                    'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image'
                ]
            );
        }
    }
}